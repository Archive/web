<!-- begin content pages -->
<!-- do not edit these lines -->
<!--#include virtual="../include/top-w-menu.shtml" -->
<!-- =========================  -->


People frequently ask about the relationship between the GNOME Project
and window managers.  GNOME does not specify a particular window
manager.  It is intended that any window manager can be used.  The
reason for this decision is that many people are attached to their
particular window manager; forcing them to switch just to use GNOME
would be counterproductive.<p>

However, to work well with Gnome, a window manager must provide
certain features which currently are not implemented in all window
managers.<p>

A GNOME-compliant window manager should implement the MWM extended
window manager hints.  Some GNOME applications will use these hints to
increase usability.  Here is a <a href="wm-hints.shtml">proposal</a>
showing how to implement these hints.<p>

There is also another <a
href="http://freeweb.pdq.net/redline/wmhints/">proposal</a>, from
Marko Macek &lt;Marko.Macek@snet.fri.uni-lj.si&gt;, for extended
window manager hints.  These hints are intended to supplement, and not
replace, the MWM hints.<p>

Last, a GNOME window manager should also be a client of the session
manager, following the <a
href="ftp://ftp.xfree86.org/pub/XFree86/current/untarred/xc/doc/specs/SM/">X
Session Manager Protocol</a>.  This is a requirement for a window
manager to be considered even minimally Gnome-compliant.<p>

It's possible that these hints are insufficient or incorrect in some
way.  We encourage discussion of the extensions and additions on the
<a href="mailto:gnome-list@gnome.org">Gnome mailing list</a>.<p>


<!-- do not edit below this -->
<!--#include virtual="../include/bottom-w-menu.shtml" -->
<!--  =================== -->
<!-- end of content page   -->
