GNOME Release 0.12 Now Available

The GNOME team is happy to present release 0.12 of the GNOME
desktop environment.  Please, read the instructions that follow.

<p> This time, instead of boring you with a list of requirements, we are
going to start with the list of new features and the boring
information bits are at the end.
  
<p> We have got contribution from a lot of people this time: Radek Doulik,
Alan Cox, Tom Tromey, Elliot Lee, George Lebl, Raja Harinath,
Eckehard Berns, Changwoo Ryu, Michael K Johnson, Federico Mena, Nathan
Bryant, Horacio Pea, and the "make dist; make install" man.
  
<p> We are very thankful to Debian for hosting our CVS repository which
has definitely helped us work much faster than we had expected.

%FULLVERSION

<ul>
<li>New Programs
<ul>
        <li> gtop: Radek Doulik's system status monitor for Linux systems.

        <li> Alan Cox's ScottsFree: an interface to let you play Scott Adam
          games (if you have Red Hat, install the mysterious RPM and
          then try something like:
<ul>
            <li>    gscott /usr/lib/games/scottfree/1_baton.dat
</ul>
        <li> Gnome Doom!  Alan ported the now freely available
          Doom source to Gnome.  It currently has a bunch of bugs
          and runs only on 16bpp, but it is a start.

          Since Doom is not completely free software, we have placed
          Gnoom in a separate tar file and is not fully autoconf/automake
          integrated.  You need the wad files from your favorite Doom
          to play.

        <li> FreeCell: The game of freecell is now included in the
          distribution (Changwoo Ryu, which reminds me: the card
          bitmaps should probably be made a library, so that any
          random hacker can use those bitmaps for his own game).

        <li>  GnomeStock: this is a library: it comes with a bunch of
          icons ready for you to use in your favorite application.
          with default for various operations.
</ul>
<li> Enhanced toys:
<ul>
        <li> Panel has been enhanced a lot by George Lebl and Ian Slow,
          it is now possible to configure the panel (position on the
          screen, kind of placement, animation delays, and moreand it
          is even usable. You want to try the new panel.  
	You want it.  You want it.

        <li> GTT: The time tracker keeps getting better and better, it
          now has on-line help using our XmHTML ported widget and has
          documentation as well (we still need those DocBook guys to
          help us with this).
          It has right-menu context sensitive menus as well and log
          files.

        <li> Panel's mail check applet is now configurable (Miguel de Icaza).

        <li> The printer control application has got a nicer interface
          (Elliot, Federico)

        <li> Tim Janik's GemVT widget/app has been updated, should be
          speedier now.

        <li> XmHTML port: much better: color handling is done properly;
          links get highlighted in cool ways;  lots of bugs were
          corrected. If you are using Gtk/XmHTML, you need to upgrade, it is
          worth it.

        <li> LinuxConf frontend: it is almost done.

        <li> Genius calculator has been expanded and extended a lot:
          internal functions, simple loops, and more.

        <li> ObGtk/Gnome: they are synced to the latest Gtk+ and Gnome
          libs.
</ul>
<li>Translations
<ul>
        <li> New! We have the translations to several languages.  
	Unfortunately, they don't
          work.  As usual, we appreciate your help to fix this.
</ul>
<li> Bugs fixed:
<ul>
        <li> The "-lintl" problem should be gone now.

        <li> Many.  Really.
</ul>
<li>How to get it
<ul>
<li>
Available by FTP from <a href="ftp://ftp.nuclecu.unam.mx/GNOME/">
ftp://ftp.nuclecu.unam.mx/GNOME/</a>
<li>
Available from Anonymous CVS:
<pre>
$ export CVSROOT=':pserver:anonymous@cvs.gimp.org:/debian/home/gnomecvs'
$ cvs login  (there is no password, just hit return)
$ cvs -z9 checkout gnome
</pre>
</ul>
<li> Requirements:
<ul>
        <li> Guile 1.2: FSF's scheme interpreter.
<br><a href="ftp://prep.ai.mit.edu/pub/gnu/guile-1.2.tar.gz">
ftp://prep.ai.mit.edu/pub/gnu/guile-1.2.tar.gz</a>

        <li> GNU gettext:
<br><a href="ftp://prep.ai.mit.edu/pub/gnu">
ftp://prep.ai.mit.edu/pub/gnu</a>

        <li> Gtk+ 0.99.3
<br><a href="ftp://ftp.gimp.org/pub/gtk/">
ftp://ftp.gimp.org/pub/gtk/</a>

        <li> SLIB 2b3
<br><a href="http://www-swiss.ai.mit.edu/~jaffer/SLIB.html">
http://www-swiss.ai.mit.edu/~jaffer/SLIB.html</a>

        <li> desktop_properties:  This program requires that you have
          installed the latest BETA of the xlockmore program,
          available from:
<br><a href="ftp://wauug.erols.com/pub/X-Windows/xlockmore/index.html">
ftp://wauug.erols.com/pub/X-Windows/xlockmore/index.html</a>        

        <li> libXpm.
</ul>
<li>Optional libraries that might be used during compile:
<ul>
        <li> JPEG, PNG and zlib are optionally used by the HTML display
          engine.  If found the engine will support those image formats.
</ul>
</ul>

