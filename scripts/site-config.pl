package GnomeWWW::SiteConfig;
use vars qw[$WEB_SOURCE_ROOT $VIRTUAL_ROOT $MASTER_ROOT $DEST_ROOT];
use strict;

# Where the checked out copy of the `web' module lies.
$WEB_SOURCE_ROOT = q[/pub/www/gnome/web];

# Server Root; http://newsite.gnome.org/ will point to this directory.
$VIRTUAL_ROOT = q[/pub/www/newsite.gnome.org];

$MASTER_ROOT = q[/pub/www/www.gnome.org];
$DEST_ROOT = q[/pub/www/newsite.gnome.org];

