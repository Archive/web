#
# $Id$
#
# Author: James G. Smith
#
# Copyright (C) 1999
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc.,
# 675 Mass Ave, Cambridge, MA 02139, USA.
#
# The author may be reached at <j-smith@physics.tamu.edu>
# or 1017 Winding Rd., College Station, TX 77840
#

package CGI::WeT::Engine;

#use strict;
use Carp;
use vars qw($VERSION);

$VERSION = '0.6';

sub new {
    my $this = shift;
    my $class = ref($this) || $this;
    my ($args, $in, @in, %in, $key, $val, $i, %cookiein, $k);

    my $self = {};

    bless $self, $class;

    $$self{MOD_PERL} = ($ENV{MOD_PERL} =~ /mod_perl\/([.0-9]+)/)[0] || 0;

    $$self{CONTENT} = [ ];

    if($ENV{REQUEST_METHOD} eq 'GET') {
        $in = $ENV{QUERY_STRING};
    } elsif($ENV{REQUEST_METHOD} eq 'POST') {
        read(STDIN, $in, $ENV{CONTENT_LENGTH});
    } else {
        $in = '';
    }
    @in = split(/[&;]/,$in);
    foreach $i (0 .. $#in) {
        $in[$i] =~ s/\+/ /g;

        ($key, $val) = split(/=/,$in[$i],2);

        $key =~ s/%([A-Fa-f0-9]{2})/pack("c",hex($1))/ge;
        $val =~ s/%([A-Fa-f0-9]{2})/pack("c",hex($1))/ge;
        $val =~ s/[\r\n]+/ /g;
        $in{$key} .= " " if (defined($in{$key}));
        $in{$key} .= $val;
    }
    if($ENV{HTTP_COOKIE}) {
        $in = $ENV{HTTP_COOKIE};
        @in = split(/[&;]/,$in);
        foreach $i (0 .. $#in) {
            $in[$i] =~ s/\+/ /g;

            ($key, $val) = split(/=/,$in[$i],2);

            $key =~ s/%([A-Fa-f0-9]{2})/pack("c",hex($1))/ge;
            $val =~ s/%([A-Fa-f0-9]{2})/pack("c",hex($1))/ge;
            $val =~ s/[\r\n]+/ /g;
            $cookiein{$key} .= " " if (defined($cookiein{$key}));
            $cookiein{$key} .= $val;
        }
        foreach $k (keys %cookiein) {
            $in{$k} ||= $cookiein{$k};
        }
    }

    $self->arguments_push(\%in);


    return $self;
}

sub content_pop {
    my $self = shift;

    return pop @{ $$self{CONTENT} };
}

sub content_push {
    my $self = shift;

    push @{ $$self{CONTENT} }, @_;
}

sub arguments_pop {
    my $self = shift;

    return pop @{ $$self{ARGUMENTS} };
}

sub arguments_push {
    my $self = shift;

    push @{ $$self{ARGUMENTS} }, @_;
}

sub argument {
    my $self = shift;
    my $arg = shift;
    my ($i, $n);

    use integer;

    if(defined $$self{ARGUMENTS}) {
        $i = scalar(@{ $$self{ARGUMENTS} });
        $n = $i-1;
        while($i) {
            $i--;
            if(exists $$self{ARGUMENTS}->[$i]->{$arg}) {
                $$self{ARGUMENTS}->[$n]->{$arg} ||=
                    $$self{ARGUMENTS}->[$i]->{$arg};
                return($$self{ARGUMENTS}->[$i]->{$arg});
            }
        }
    }
    return undef;
}

sub headers_push {
    my $self = shift;
    my $k;

    $$self{HEADERS} ||= { };

    while (scalar(@_)) {
        $k = shift;
        $$self{HEADERS} -> {$k} = shift;
    }
}

sub body_push {
    my $self = shift;

    push @{ $$self{BODY} }, @_;
}

sub render_content {
    my $self = shift;

    use integer;

    my $layout = $self->content_pop;
    my(@output);
    my($position) = (0);
    my($args, $key, $val, $object);

    return () if !defined $layout;

    while($position < scalar(@$layout)) {
	if(ref($layout->[$position])) {
            $self->content_push($layout->[$position]);
        } else {
            if($layout->[$position] =~ /^\[([A-Z_]+)\s*(.*)\]$/) {
                $object = $1;
                $args = { };
                foreach (split(/\s+/, $2)) {
                    ($key, $val) = split(/=/, $_, 2);
                    $val =~ s/%(..)/pack("c", hex($1))/ge;
                    $$args{$key} = $val;
                }
                $self->arguments_push($args);
                if(defined $ { "CGI::WeT::Themes::" . $self->argument('theme')
				   .  "::$object\::LAYOUT" }) {
                    $self->content_push($ { "CGI::WeT::Themes::" .
					    $self->argument('theme') .
						"::$object\::LAYOUT"});
                    push(@output, $self->render_content);
                } elsif(defined & { "CGI::WeT::Modules::$object" }) {
                    push(@output, & { "CGI::WeT::Modules::$object" }($self));
                }
            } else {
                push(@output, $layout->[$position]);
            }
        }
	$position++;
    }
    return @output;
}

sub render_page {
    my $self = shift;
    my(@output);
    my($theme, $layout, $css);

    push(@output, "<html>", "<head>");
    push(@output,
         map("<meta name=\"$_\" content=\"$$self{HEADERS}->{$_}\">",
             grep(defined $$self{HEADERS}->{$_},
                  'Author', 'Keywords', 'Date'
                  )
             )
         );
    push(@output, "<meta name=\"Generator\" content=\"WeT Perls $CGI::WeT::Engine::VERSION\">");
    push(@output, "<meta name=\"Theme\" content=\"", $self->argument('theme'),
	 "\">");

    push(@output, "<title>", "$$self{SITENAME} - $$self{HEADERS}->{Title}",
         "</title>");

    foreach (grep(!/^\s*$/, 
			 split(/\s+/, $self->{HEADERS}->{Type}), 'DEFAULT')) {
	next if !defined $ { "CGI::WeT::Themes::" . $self->argument('theme') . 
			       "::$_\::LAYOUT" };
	$theme = $_;
	$layout ||= $ { "CGI::WeT::Themes::" . $self->argument('theme') . 
			    "::$theme\::LAYOUT" };

       last if defined $layout;
    }

    push(@output, "<style type=\"text/css\">", "<!-- ");
    $css = $self->{'DOCUMENT_ROOT'} . '/' . $self->{'THEMEDIR'} . "/css/$theme\.css";
    if(-e $css) {
        open IN, "<$css";
        $css = join("", (<IN>));
        close IN;
        $css =~ s/url\((.*?)\)/url\($self->{'THEMEDIR'}\/$1\)/;
        push(@output, $css);
    }
    push(@output, " -->","</style>", "</head>");
    if(defined ( 
		$ { "CGI::WeT::Themes::" . $self->argument('theme') .
			"::$theme\::BODY" }) ) {
	my($bodyinfo) = 
	    $ { "CGI::WeT::Themes::" . $self->argument('theme') . 
		    "::$theme\::BODY"};
	push(@output, "<body");
	push(@output, " background=\"$bodyinfo->{'background'}\"")
	    if exists $bodyinfo->{'background'};
	push(@output, " bgcolor=\"#$bodyinfo->{'bgcolor'}\"")
	    if exists $bodyinfo->{'bgcolor'};
	push(@output, " text=\"#$bodyinfo->{'text'}\"")
	    if exists $bodyinfo->{'text'};
	push(@output, " link=\"#$bodyinfo->{'link'}\"")
	    if exists $bodyinfo->{'link'};
	push(@output, " vlink=\"#$bodyinfo->{'vlink'}\"")
	    if exists $bodyinfo->{'vlink'};
	push(@output, " alink=\"#$bodyinfo->{'alink'}\"")
	    if exists $bodyinfo->{'alink'};
	push(@output, ">");
    } else {
	push(@output, "<body>");
    }

    $self->content_push($layout);
    push(@output, $self->render_content);
    push(@output, "</body>","</html>");
    return @output;
}

sub handler {
    my $r = shift;
    my $filename = $r->filename;
    my $engine = new CGI::WeT::Engine;
    my ($key, $val);

    if(-e $r->finfo) {
        open IN, "<$filename";
        while(<IN>) {
            last if /^\s*$/;
            next if /^\s*#/;
            chomp;
            if(/^[A-Za-z_]+:/) {
                ($key, $val) = split(/:/,$_,2);
            } else {
                $val = " $_";
            }
            $engine->headers_push($key, $val);
        }

        #
        # slurp up the rest of the file
        #
        $engine->body_push((<IN>));
        close IN;
    }

    $r->print( $engine->render_page );
}

