#!/usr/bin/perl -w

require 5.004;
use strict;
use Carp;

require 'site-config.pl';
require 'apache-ssi.pl';

sub scan_directory {
  my ($dir) = @_;

  opendir MASTER, qq[$GnomeWWW::SiteConfig::MASTER_ROOT/$dir] or
    croak "opendir ($GnomeWWW::SiteConfig::MASTER_ROOT/$dir): $!";
  my @readdir = readdir MASTER or
    croak "readdir ($GnomeWWW::SiteConfig::MASTER_ROOT/$dir): $!";
  closedir MASTER;

  my @files = grep { ! /^[\.\#]/ && /\.shtml$/ } @readdir;
  my @directories = grep { 
    ! /^\./ && -d qq[$GnomeWWW::SiteConfig::MASTER_ROOT/$dir/$_]
  } @readdir;

  unless (-d qq[$GnomeWWW::SiteConfig::DEST_ROOT/$dir/.deps]) {
    mkdir qq[$GnomeWWW::SiteConfig::DEST_ROOT/$dir/.deps], 0755 or
      croak "mkdir ($GnomeWWW::SiteConfig::DEST_ROOT/$dir/.deps): $!";
  }

  foreach my $file (@files) {
    my $check = &GnomeWWW::SSI::check ($dir, $file);
    next if ($check <= 0);

    &GnomeWWW::SSI::make ($dir, $file);
  }

  foreach my $newdir (@directories) {
    print STDERR "Entering directory $newdir ...\n";
    &scan_directory (qq[$dir/$newdir]);
    print STDERR "Leaving directory $newdir ...\n";
  }
}

&scan_directory ('.');
