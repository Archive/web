#!/usr/bin/perl -w

require 5.004;
use strict;
use Carp;

require 'site-config.pl';
require 'apache-ssi.pl';

my $input = join '', <STDIN>;
my @output = &GnomeWWW::SSI::parse ($input);
print $output[0];

