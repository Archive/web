#! /usr/bin/perl

use strict;
require './theme-engine-bd.pl';

#
# $Id$
#
# Author: James G. Smith
#
# Copyright (C) 1999
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc.,
# 675 Mass Ave, Cambridge, MA 02139, USA.
#
# The author may be reached at <j-smith@physics.tamu.edu>
# or 1017 Winding Rd., College Station, TX 77840
#

#
# This script should be called with a list of arguments 
#

my($content_list, $content_ref);

while(@ARGV) {
    my($layout, %body_config);
    my($file, $l);
    my($master_file) = shift(@ARGV);
    my($relative_url) = '';
    my($this_url);
    my($frame_contents);
    my($toparguments) = {
	'theme' => 'Plain'
	};
    my $engine = new CGI::WeT::Engine;

    $content_list = [ ];
    $content_ref = { };

    $engine->arguments_push($toparguments);

    undef $/;
    open IN, "<$master_file";
    $file = <IN>;
    close IN;
    
    $relative_url = $1 if($master_file =~ /^(.*)\/[^\/]+$/);

    $file =~ tr/\n\r/  /;
    $file =~ /<\s*html\s*>(.*)<\s*\/html\s*>/i;
    $file = $1;

    if($file =~ /<\s*head.*?>(.*)<\s*\/head\s*>/i) {
	my($head) = $1;
	$engine->headers_push('Title' => $1) 
	    if $head =~ /<\s*title\s*>(.*)<\s*\/title\s*>/i;
    }

    if($file =~ /(<\s*body.*?>)(.*)<\s*\/body\s*>/i) {
	my($bodytag) = $1;
	study $bodytag;
	$engine->body_push($2);
	$body_config{'background'} = $1
	    if $bodytag =~ /\bbackground=\"?([^\s\"]+)\"?/i;
	$body_config{'bgcolor'} = $1
	    if $bodytag =~ /\bbgcolor=\"?#([0-9a-f]{6})\"?/i;
	$body_config{'alink'} = $1
	    if $bodytag =~ /\balink=\"?([0-9a-f]{6})\"?/i;
	$body_config{'vlink'} = $1
	    if $bodytag =~ /\bvlink=\"?([0-9a-f]{6})\"?/i;
	$body_config{'link'} = $1
	    if $bodytag =~ /\blink=\"?([0-9a-f]{6})\"?/i;
	$body_config{'text'} = $1
	    if $bodytag =~ /\btext=\"?([0-9a-f]{6})\"?/i;
    }

    #
    # build up the format
    #

    if($file =~ /(<\s*frameset.*?>.*<\s*\/frameset\s*>)/i) {
	my($frames) = $1;

	($layout, $frames) = &doFrames($frames);
    } else {
	$layout = [ '[BODY]' ];
    }

    $CGI::WeT::Themes::Plain::DEFAULT::LAYOUT = $layout;
    $CGI::WeT::Themes::Plain::DEFAULT::BODY = \%body_config;

    foreach $l (reverse @$content_list) {
#	print "(L", __LINE__, ") $l :> $$content_ref{$l}\n";
	open IN, "<$relative_url/$$content_ref{$l}";
	$frame_contents = <IN>;
	close IN;

	$this_url = $1 if($$content_ref{$l} =~ /^(.*)\/[^\/]+$/);

	
	$frame_contents =~ tr/\n\r/  /;
	$frame_contents =~ s/<\s*head.*?>.*<\s*\/head.*?>//ig;
        $frame_contents =~ s/<\s*\/?html.*?>//ig;
        $frame_contents =~ s/<\s*\/?body.*?>//ig;
	my(@urls) = $frame_contents =~ m!href=\"([^/].*?)\"!ig;
	foreach (grep(!/^[a-z]+:\/\//, @urls)) {
	    $frame_contents =~ s/href=\"$_\"/href=\"$this_url\/$_\"/g;
	}
	my(@urls) = $frame_contents =~ m!src=\"([^/].*?)\"!ig;
	foreach (grep(!/^[a-z]+:\/\//, @urls)) {
	    $frame_contents =~ s/src=\"$_\"/src=\"$this_url\/$_\"/g;
	} 
#	print "(L", __LINE__, ") $relative_url/$$content_ref{$l}\n$frame_contents\n";
	$engine->content_push([ [ $frame_contents ], '[TEXT]' ]);
    }

    print $engine->render_page();
    print "\n";
}

sub doFrames {
    my($frames) = shift;
    my($args, $key, $val);
    my(@rows, @cols);
    my($layout, $i);
    my($t1, $vbox, $line);
    #
    # we are going to do this brute force -- nothing elegent here
    #
    $frames =~ /.*?<\s*frameset\s*(.*?)>/i;
    $args = { };
    foreach (split(/\s+/, $1)) {
	($key, $val) = split(/=/, $_, 2);
	$val =~ s/\"//g;
	$$args{"\L$key"} = $val;
    }

    $frames =~ s/.*?<\s*frameset\s*.*?>//i;
    $$args{'rows'} =~ s/%/%25/g;
    $$args{'cols'} =~ s/%/%25/g;
    @rows = split(/\s*,\s*/, $$args{'rows'} || "*");
    @cols = split(/\s*,\s*/, $$args{'cols'} || "*");
    
    #
    # @rows is only useful to determine how many rows ([LINE]) there are
    #

    $line = [ ];
    for (0..$#rows) {
	$vbox = [ ];
	for $i (0..$#cols) {
	    if($frames =~ /^\s*<\s*frame\b\s*(.*?)\s*>/i) {
		$args = { };
		foreach (split(/\s+/, $1)) {
		    ($key, $val) = (split(/=/, $_, 2));
		    $val =~ s/\"//g;
		    $$args{"\L$key"} = $val;
		}
		$frames =~ s/^\s*<\s*frame\b.*?>//i;
		push(@$vbox, 
		     [ '[CONTENT]' ], 
		     "[VBOX" . 
		     ($cols[$i] eq '*' ? '' : " width=$cols[$i]") .
		     "]");
		push(@$content_list, $$args{'name'});
		$$content_ref{$$args{'name'}} = $$args{'src'};
#		print "(L", __LINE__, ") $$args{'name'} :> $$args{'src'}\n";
	    } elsif($frames =~ /^\s*<\s*frameset\b/i) {
		($t1, $frames) = &doFrames($frames);
		push(@$vbox,
		     $t1,
		     "[VBOX" . 
		     ($cols[$i] eq '*' ? '' : " width=$cols[$i]") .
		     "]");
	    }
	}
	push(@$line, $vbox, '[LINE]');
    }
    push(@$layout, $line, '[HBOX border=0 width=100%25]');
    $frames =~ s/.*?<\s*\/frameset\s*>//;
    return ($layout, $frames);
}

sub CGI::WeT::Modules::BODY {
    my $engine = shift;

    return join("\n", @ { $engine->{BODY} } );
}

sub CGI::WeT::Modules::HBOX {
    my $engine = shift;

    my(@output);

    push(@output, "<table");
    foreach ('width', 'cellspacing',' cellpadding', 'border',
	     'height') {
	push(@output, " $_=", $engine->argument($_))
	    if $engine->argument($_) !~ /^\s*$/;
    }
   
    push(@output, " bgcolor=\"#", $engine->argument('bgcolor'), "\"")
	if defined $engine->argument('bgcolor');
    push(@output, ">");
    push(@output, $engine->render_content);
    push(@output, "</table>");
    return @output;
}

sub CGI::WeT::Modules::VBOX {
    my $engine = shift;

    my(@output);
    
    push(@output, "<td");
    foreach ('width', 'valign', 'colspan', 'rowspan', 'height', 'align',
	     'background') {
	push(@output, " $_=", $engine->argument($_))
	    if $engine->argument($_) !~ /^\s*$/;
    }
    
    push(@output, " bgcolor=\"#", $engine->argument('bgcolor'), "\"")
	if defined $engine->argument('bgcolor');
    push(@output, ">");
    push(@output, $engine->render_content);
    push(@output, "</td>");
    return @output;
}

sub CGI::WeT::Modules::LINE {
    my $engine = shift;

    return ("<tr>", $engine->render_content, "</tr>");
}

sub CGI::WeT::Modules::CONTENT {
    my $engine = shift;

    return($engine->render_content);
}

sub CGI::WeT::Modules::TEXT {
    my $engine = shift;

    return join(" ", @ { $engine->content_pop });
}
