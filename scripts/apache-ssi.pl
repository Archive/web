package GnomeWWW::SSI;

require 5.004;
use strict;
use Carp;

sub check {
  my ($dir,$file) = @_;

  local $SIG{__WARN__} = sub {
    my $message = $_[0]; chop $message;
    print STDERR "$file: $message.\n";
  };

  my $stem = $file; $stem =~ s/\.shtml$//;
  my $shtml_file = qq[$GnomeWW::SiteConfig::MASTER_ROOT/$dir/$stem.shtml];
  my $html_file = qq[$GnomeWW::SiteConfig::DEST_ROOT/$dir/$stem.html];
  my $dep_file = qq[$GnomeWWW::SiteConfig::DEST_ROOT/$dir/.deps/$stem.P];
  my $shtml_base = qq[$dir/$stem.shtml];
  my $html_base = qq[$dir/$stem.html];
  my $dep_base = qq[$dir/.deps/$stem.P];
  
  my @shtml_stat = stat ($shtml_file) or do {
    carp "stat ($shtml_file): $!";
    return -1;
  };

  if (-e $html_file) {
    my @html_stat = stat ($html_file) or do {
      carp "stat ($html_file): $!";
      return -1;
    };

    # .html file newer that the .shtml file ?
    if ($shtml_stat[9] > $html_stat[9]) {
      unlink ($html_file) or carp "unlink ($html_file): $!";
      if (-e $dep_file) {
	unlink ($dep_file) or carp "unlink ($dep_file): $!";
      }
      print STDERR "$html_base is older than $shtml_base\n";
      return 1;
    }

    # Do we have a .P file ?
    if (-e $dep_file) {
      my @dep_stat = stat ($dep_file) or do {
	carp "stat ($dep_file): $!";
	return -1;
      };

      # if the .P file is older than the .shtml file we can't use it
      if ($dep_stat[9] > $html_stat[9]) {
	unlink ($dep_file) or carp "unlink ($dep_file): $!";
	if (-e $html_file) {
	  unlink ($html_file) or carp "ulink ($html_file): $!";
	}
	print STDERR "$dep_file is newer than $html_base\n";
	return 1;
      }

      # Read the .P file
      open DEP_FILE, $dep_file or do {
	carp "open ($dep_file): $!";
	return -1;
      };
      my $dep_line;
      while ($dep_line = <DEP_FILE>) {
	$dep_line =~ s/\n$//;
	my @dep_stat = stat ($dep_line) or do {
	  carp "stat ($dep_line): $!";
	  next;
	};
	# Is this dependency newer than the .shtml file ?
	if ($dep_stat[9] > $html_stat[9]) {
	  unlink ($dep_file) or carp "unlink ($dep_file): $!";
	  if (-e $html_file) {
	    unlink ($html_file) or carp "ulink ($html_file): $!";
	  }
	  print STDERR "$dep_file is newer than $html_base\n";
	  return 1;
	}
      }
      close DEP_FILE;
      return 0;
    }
  }

  return 1;
}

sub make {
  my ($dir, $file) = @_;

  local $SIG{__WARN__} = sub {
    my $message = $_[0]; chop $message;
    print STDERR "$file: $message.\n";
  };

  my $stem = $file; $stem =~ s/\.shtml$//;
  my $shtml_file = qq[$GnomeWWW::SiteConfig::MASTER_ROOT/$dir/$stem.shtml];
  my $html_file = qq[$GnomeWWW::SiteConfig::DEST_ROOT/$dir/$stem.html];
  my $dep_file = qq[$GnomeWWW::SiteConfig::DEST_ROOT/$dir/.deps/$stem.P];
  my $shtml_base = qq[$dir/$stem.shtml];
  my $html_base = qq[$dir/$stem.html];
  my $dep_base = qq[$dir/.deps/$stem.P];

  print STDERR "Creating $html_base from $shtml_base ...\n";

  open INPUT, $shtml_file or do {
    carp "open ($shtml_file): $!";
    next;
  };
  my $input = join '', <INPUT>;
  close INPUT;

  my ($output, @deps) = &GnomeWWW::SSI::parse ($input);

  open DEP_FILE, "> $dep_file" or do {
    carp "open ($dep_file): $!";
    next;
  };
  print DEP_FILE join ("\n", @deps);
  close DEP_FILE;

  open OUTPUT, "> $html_file" or do {
    carp "open ($html_file): $!";
    next;
  };
  print OUTPUT $output;
  close OUTPUT;
}

sub parse {
  my ($input) = @_;
  my %VARIABLES;
  my $OUTPUT = '';
  my @DEPS;
  
 main_loop:
  {
    last unless $input =~ /<!--\#(\w+)\b\s*(.*?)\s*-->/s;
    
    my ($command, $rest, $replacement) = ($1, $2, '');
    
    my $para_array = [];
    my $para_hash = {};
    
    $OUTPUT .= $`;
    $input = $';
    
    while ($rest =~ /^(\w+)=(\"[^\"]+\"|\S+)\s*(.*)$/) {
      my ($attribute, $value) = ($1,$2); $rest = $3;
      $value =~ s/^\"//; $value =~ s/\"$//;
      
      push @$para_array, [$attribute, $value];
      $para_hash->{$attribute} = $value;
    }
    
    if ($command =~ /^set$/) {
      redo main_loop unless defined $para_hash->{var} and
	defined $para_hash->{value};
      
      $VARIABLES{$para_hash->{var}} = $para_hash->{value};
      $input = qq[$replacement$input];
      redo main_loop;
    }

    if ($command =~ /^echo$/) {
      redo main_loop unless defined $para_hash->{var} and
	defined $VARIABLES{$para_hash->{var}};

      $input = $VARIABLES{$para_hash->{var}}.$input;
      redo main_loop;
    }
    
    if ($command =~ /^include$/) {
      redo main_loop unless defined $para_hash->{virtual};
      
      my $file = $GnomeWWW::SiteConfig::VIRTUAL_ROOT.'/'.$para_hash->{virtual};
      $file =~ s,//,/,g; $file =~ s,//,/,g;
      
      open FILE, $file or do {
	carp "open ($file): $!";
	redo main_loop;
      };
      $replacement = join '', <FILE>;
      close FILE;

      $input = qq[$replacement$input];
      push @DEPS, $file;
      redo main_loop;
    }

    carp "Unknown command `$command'";
    
    $input = qq[$replacement$input];
    redo main_loop;
  }
  
  return ($OUTPUT.$input, @DEPS);
}

1;

